package in.railway.feedback.alert;

import in.railway.feedback.report.db.DatabaseManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.dbutils.DbUtils;

public class CallsAlert {

	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null, stmt1 = null;
		ResultSet rs = null, rs1 = null;
		String date = args[0];
		// String date = "2015-08-13";
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat callTimeFormatter = new SimpleDateFormat("dd-MM-yyyy");
			String callingDate = "";
			if (date != null && !"".equals(date)) {
				callingDate = callTimeFormatter.format(formatter.parse(date));
			}

			con = DatabaseManager.getConnection();
			stmt = con.prepareStatement("SELECT SUM(calls_initiated),SUM(calls_matured),SUM(bad),SUM(below_avg),"
					+ "SUM(average),SUM(good),SUM(excellent),SUM(feedback_no_input),SUM(opted_for_feedback) FROM zone_feedback_new WHERE insert_date = ?");
			// stmt1 = con
			// .prepareStatement("SELECT COUNT(DISTINCT(train_no)) FROM
			// tbl_outdial WHERE DATE_FORMAT(STR_TO_DATE('"
			// + callingDate
			// +
			// "','%d-%m-%Y'),'%d-%m-%Y')=DATE_FORMAT(STR_TO_DATE(calling_time,'%d-%m-%Y'),'%d-%m-%Y')");
			stmt1 = con.prepareStatement(
					"SELECT COUNT(DISTINCT(train_no)) FROM tbl_outdial WHERE DATE_FORMAT(update_datetime,'%Y-%m-%d')='"
							+ date + "' and status = 'S'");
			stmt.setString(1, date);
			rs = stmt.executeQuery();
			String callsInitiated = "", callsMatured = "", bad = "", belowAvg = "", average = "", good = "",
					excellent = "", feedbackNoInput = "", trainsCovered = "", optedForFeedback = "";
			while (rs.next()) {
				callsInitiated = rs.getString(1);
				callsMatured = rs.getString(2);
				bad = rs.getString(3);
				belowAvg = rs.getString(4);
				average = rs.getString(5);
				good = rs.getString(6);
				excellent = rs.getString(7);
				feedbackNoInput = rs.getString(8);
				optedForFeedback = rs.getString(9);
			}

			rs1 = stmt1.executeQuery();

			while (rs1.next()) {
				trainsCovered = rs1.getString(1);
			}
			String message = String.format(
					"Date:%s,\nCalls Initiated:%s,\nSuccessful:%s,\nBad:%s,\nBelow Average:%s,\nAverage:%s,\nGood:%s,\nExcellent:%s,\nFeedback No Input:%s,\nOpted For Feedback:%s,\nTrains Covered:%s",
					callingDate, callsInitiated, callsMatured, bad, belowAvg, average, good, excellent, feedbackNoInput,
					optedForFeedback, trainsCovered);
			System.out.println(message);
			getSendSmsAlert(message);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(con);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt1);
			DbUtils.closeQuietly(rs1);
		}
	}

	public static void getSendSmsAlert(String strMessage) throws Exception {
		String strUrl = "";
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream(new File("/home/railway_calls_alert/numbers.properties"));
		// FileInputStream fis = new FileInputStream(new
		// File("C:/Users/ch-e00967/Desktop/numbers.properties"));
		prop.load(fis);
		// String mobileNos =
		// "9560585295,9216301253,7087561823,9855692775,9459861333,8567836652,8439505295,9914201405,9914001671";
		// // spice
		String mobileNos = prop.getProperty("numbers");
		System.out.println(mobileNos);
		String[] arrMobile = mobileNos.split(",");

		List<String> mobileNumbers = Arrays.asList(arrMobile);
		try {
			strMessage = URLEncoder.encode(strMessage, "UTF-8");
			for (String mobileNumber : mobileNumbers) {
				strUrl = "http://192.168.9.34:8085/BulkMessaging/sendingSMS?ani=91" + mobileNumber
						+ "&uname=RailFbkSMS&passwd=rfbs30thjul&cli=RBFDBK&message=" + strMessage;
				try {
					sendMessage(strUrl);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fis.close();
		}

	}

	private static String sendMessage(String strUrl) {

		BufferedReader bufferedReader = null;
		HttpURLConnection connection = null;
		InputStream is = null;
		URL url;
		String strOutPut = "", strLogString = "";
		String strInputLine = "";
		DataOutputStream wr = null;
		try {
			// strUrl = strUrl.replaceAll(" ", "%20");
			url = new URL(strUrl);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setReadTimeout(5000);
			connection.setConnectTimeout(10000);

			wr = new DataOutputStream(connection.getOutputStream());

			wr.writeBytes(strLogString);

			is = connection.getInputStream();
			bufferedReader = new BufferedReader(new InputStreamReader(is));
			while ((strInputLine = bufferedReader.readLine()) != null) {
				strOutPut += strInputLine;
			}
			System.out.println("Response XML:" + strOutPut);
		} catch (Exception e) {
			strOutPut = "";
			e.printStackTrace();

		} finally {
			if (wr != null) {
				try {
					wr.flush();
					wr.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.disconnect();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return strOutPut;
	}

}
